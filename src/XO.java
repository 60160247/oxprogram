import java.util.*;

public class XO {
	static char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static char turn = 'x';
	static int count = 0;
	static Scanner ip = new Scanner(System.in);

	public static void showWelcome() {
		System.out.println("Welcome to XO Game");
	}

	public static void showTurn() {
		if (turn == 'x') {
			System.out.println("X Turn");
		} else {
			System.out.println("O Turn");
		}
	}

	public static void showTable() {
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print((i + 1) + " ");
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void switchTurn() {
		if (turn == 'x') {
			turn = 'o';
		} else {
			turn = 'x';
		}
	}

	public static boolean checkWin() {
		if (table[0][0] == turn && table[0][1] == turn && table[0][2] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[1][0] == turn && table[1][1] == turn && table[1][2] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[2][0] == turn && table[2][1] == turn && table[2][2] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[0][0] == turn && table[1][0] == turn && table[2][0] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[0][1] == turn && table[1][1] == turn && table[2][1] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[0][2] == turn && table[1][2] == turn && table[2][2] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else if (table[0][2] == turn && table[1][1] == turn && table[2][0] == turn) {
			System.out.println(turn + " Win!!");
			return true;
		} else {
			return false;
		}
	}

	public static void inputRowCol() {
		System.out.print(turn + " row col : ");
		int row = ip.nextInt();
		int col = ip.nextInt();
		for (;;) {
			if (row < 1 || row > 3 || col < 1 || col > 3) {
				showTable();
				System.out.println("Please input row and col again!!! ");
				System.out.print(turn + " row col : ");
				row = ip.nextInt();
				col = ip.nextInt();
			} else if (!(table[row - 1][col - 1] == '-')) {
				showTable();
				System.out.println("Please input row and col again!!! ");
				System.out.print(turn + " row col : ");
				row = ip.nextInt();
				col = ip.nextInt();
			} else {
				break;
			}
		}
		table[row - 1][col - 1] = turn;
		count++;
	}

	public static boolean checkDraw() {
		if (count == 9) {
			System.out.println("Draw!!");
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		showWelcome();
		showTable();
		showTurn();
		for (;;) {
			switchTurn();
			if (turn == 'x') {
				inputRowCol();
				showTable();
				if (checkDraw()) {
					break;
				}
				if (checkWin()) {
					break;
				}
			}
			if (turn == 'o') {
				inputRowCol();
				showTable();
				if (checkDraw()) {
					break;
				}
				if (checkWin()) {
					break;
				}
			}
		}

	}

}
