
public class Board {
	private char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };

	private Player x, o, winner, current;
	private int turnCount;

	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turnCount = 0;

	}

	public char[][] getTable() {
		return table;

	}

	public Player getCurrent() {
		return current;
	}

	public boolean setTable(int row, int col) {
		if (table[row - 1][col - 1] != '-') {
			System.out.println("That position is not empty!!!");
			System.out.print("Input Row(1-3) Col(1-3) again : ");
			return false;
		} else {
			table[row - 1][col - 1] = current.getName();
			return true;
		}
	}

	public void setCurrent(Player p) {
		current = p;
	}

	public void turn() {
		turnCount++;
	}

	public int getTurn() {
		return turnCount;
	}

	public void setWinner(Player p) {
		winner = p;
	}

	public Player getWinner() {
		return winner;
	}

	public void newTable() {
		char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	}

	public void updateScore() {
		if (winner == null) {
			x.draw();
			o.draw();
		} else if (winner == x) {
			x.win();
			o.lose();
		} else {
			o.win();
			x.lose();
		}
	}

	public void showScore() {
		System.out.println("+++++++++++++++++++++++++++++++++++++");
		System.out.println("X SCORE");
		System.out.println("WIN : " + x.getWin());
		System.out.println("DRAW : " + x.getDraw());
		System.out.println("LOSE : " + x.getLose());
		System.out.println("+++++++++++++++++++++++++++++++++++++");
		System.out.println("O SCORE");
		System.out.println("WIN : " + o.getWin());
		System.out.println("DRAW : " + o.getDraw());
		System.out.println("LOSE : " + o.getLose());
		System.out.println("+++++++++++++++++++++++++++++++++++++");
	}

}
