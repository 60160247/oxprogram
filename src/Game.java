import java.util.*;

public class Game {
	private Board board;
	private Player x, o;

	public Game() {
		o = new Player('O');
		x = new Player('X');
		board = new Board(x, o);
	}

	public void play() {
		showWelcome();
		for (;;) {
			showTable();
			showTurn();
			inputRowCol();
			if (checkWin() == true) {
				showTable();
				newMatch();
			} else {
				switchTurn();
			}
		}

	}

	private void showWelcome() {
		System.out.println("Welcome to XO Game");
	}

	private void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print((i + 1) + " ");
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}

	}

	private void showTurn() {
		System.out.println("====== " + board.getCurrent().getName() + " TURN ======");
	}

	private void inputRowCol() {
		Scanner ip = new Scanner(System.in);
		System.out.print("Input Row(1-3) Col(1-3) : ");
		for (;;) {
			int row = ip.nextInt();
			int col = ip.nextInt();
			if (row < 1 || row > 3 || col < 1 || col > 3) {
				System.out.print("Input Row(1-3) Col(1-3) again : ");
				continue;
			} else {
				if (board.setTable(row, col) == true) {
					break;
				} else {
					continue;
				}
			}
		}
	}

	private boolean checkWin() {
		char[][] table = board.getTable();
		char turn = board.getCurrent().getName();
		int win = 0;
		if (board.getTurn() == 8) {
			win = 2;
		} else if (table[0][0] == turn && table[0][1] == turn && table[0][2] == turn) {
			win = 1;
		} else if (table[1][0] == turn && table[1][1] == turn && table[1][2] == turn) {
			win = 1;
		} else if (table[2][0] == turn && table[2][1] == turn && table[2][2] == turn) {
			win = 1;
		} else if (table[0][0] == turn && table[1][0] == turn && table[2][0] == turn) {
			win = 1;
		} else if (table[0][1] == turn && table[1][1] == turn && table[2][1] == turn) {
			win = 1;
		} else if (table[0][2] == turn && table[1][2] == turn && table[2][2] == turn) {
			win = 1;
		} else if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
			win = 1;
		} else if (table[0][2] == turn && table[1][1] == turn && table[2][0] == turn) {
			win = 1;
		} else {
			win = 0;
		}
		if (win == 1) {
			System.out.println("******* " + board.getCurrent().getName() + " WIN *******");
			board.setWinner(board.getCurrent());
			return true;
		} else if (win == 2) {
			System.out.println("******* DRAW *******");
			board.setWinner(null);
			return true;
		} else {
			return false;
		}
	}

	private void switchTurn() {
		if (board.getCurrent().getName() == 'X') {
			board.setCurrent(o);
			board.turn();
		} else {
			board.setCurrent(x);
			board.turn();
		}
	}

	private void newMatch() {
		board.updateScore();
		board.showScore();
		board = new Board(x, o);
	}

}
